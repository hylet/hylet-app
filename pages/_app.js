import 'antd/dist/antd.css';
import 'bootstrap/dist/css/bootstrap.css';
import '../styles/globals.css'
import '../styles/color.css'
import '../styles/nav.css';
import Layout from '../components/Layout/Layout'
import Head from 'next/head'
import {useRouter} from 'next/router';
import {useEffect,useState} from 'react';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import Router from 'next/router'

NProgress.configure({ showSpinner: false, trickleRate: 0.1, trickleSpeed: 300 });
Router.events.on('routeChangeStart', () => {NProgress.start()});Router.events.on('routeChangeComplete', () => {NProgress.done();});Router.events.on('routeChangeError', () => {NProgress.done();});

function MyApp({ Component, pageProps }) {

  const Router  = useRouter();
  const [showLayout,setShowLayout] = useState(false);

  // hides the layout when the auth page loads
  useEffect(()=>{
    const path = Router.pathname;
    if(path === '/auth'){
      setShowLayout(false);
    }else{
      setShowLayout(true);
    }
  },[Router.pathname])

  return (
      <>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <title>hylet - Marketing Simplified </title>
            <link rel="icon" href="/hylet_logo.png" />
        </Head>
        {showLayout &&  <Layout><Component {...pageProps} /></Layout> }
        {!showLayout &&  <Component {...pageProps} /> }
        
      </>
  )

}

export default MyApp
