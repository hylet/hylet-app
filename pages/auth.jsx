import React,{useState,useEffect} from 'react';
import Image from 'next/image';
import textLogo from '../assets/img/hylet_png.png';
import SignIn from '../components/Auth/SignIn'
import SignUp from '../components/Auth/SignUp';
import { useRouter } from 'next/router';

const Auth = (props)=> {
    const [showLogin,setShowLogin] = useState(true);
    const Router = useRouter();

    useEffect(() => {
        const type = Router.query.type;
        if(type === 'signup'){
            toggleLogin();
        }
    },[])

    const toggleLogin = ()=>{
        setShowLogin(!showLogin);
    }
    return (
        <div className='container-fluid h-100'>
            <div className="row h-100">
                <div className="col-7 px-0 d-none d-lg-block h-100">
                    <div className='auth-bg-img' />
                </div>
                <div className="col-12 col-lg-5">
                    <div className="mt-4">
                        <div className="text-center px-4">
                            <Image src={textLogo} height={80} width={120} alt= 'logo' />
                            {showLogin && <SignIn toggleLogin={toggleLogin} />}
                            {!showLogin && <SignUp toggleLogin={toggleLogin} />}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Auth;