import React,{useState} from 'react';
import Head from "next/head";
import OfferList from '../../components/Offers/OfferList';

const Offers = (props)=> {
    const [visible,setVisible] = useState(false);
    
    const showDrawer = () => {
        setVisible(true);    
    };
    
    const onClose = () => {
        setVisible(false)
    };
    return (
        <div className='px-2'>
            <Head>
                <title>Hylet - My Offers</title>
                <meta name="Hylet" content="Marketing Simplified" />
                <link rel="icon" href="/hylet_logo.png" />
            </Head>
            <div className='titlebar rounded'>
                <div className="d-flex justify-content-between">
                    <div>
                        <div className='h5'>My Offers</div>
                        <small className='text-secondary'>List of all available offers</small> 
                    </div>    
                </div>
            </div>
            <div className="mt-4">
                <OfferList />
            </div>
        </div>
    );
}

export default Offers;