import React,{useState} from 'react'
import Head from "next/head";
import { Tabs,Button,Menu, Dropdown,Badge,Modal,Form, Input, } from 'antd';
import { MoreOutlined  } from '@ant-design/icons';
import Image from 'next/image';
import mock from '../../../assets/img/mock.png'
import OfferGeneral from '../../../components/Offers/OfferGeneral';
const { TabPane } = Tabs;

const OfferView = ()=> {
    
    const [isRejectVisible, setRejectVisible] = useState(false);
    const [isAcceptVisible, setAcceptVisible] = useState(false);

    const showRejectModal = () => {
      setRejectVisible(true);
    };
  
    const hideRejectModal = () => {
      setRejectVisible(false);
    };

    const showAcceptModal = () => {
        setAcceptVisible(true);
      };
    
      const hideAcceptModal = () => {
        setAcceptVisible(false);
      };

    const onRejectFinish = (values)=>{console.log(values);}
    const onRejectFinishFailed = (err)=>{console.log(err);}

    const onAcceptFinish = (values)=>{console.log(values);}
    const onAcceptFinishFailed = (err)=>{console.log(err);}
      
    return (
        <div className='px-2 '>
            <Head>
                <title>Hylet - New Launch</title>
                <meta name="Hylet" content="Marketing Simplified" />
                <link rel="icon" href="/hylet_logo.png" />
            </Head>
            <div className='titlebar rounded'>
                <div className="row ">
                    <div className="col-lg-6 col-12">
                        <div className={'px-1 mt-3'}>
                            <div className='d-flex justify-content-between'>
                                <div className='d-flex justify-content-between'>
                                    <Image preview={"false"} className={'rounded'} height='50' width='50'  alt='logo' src={mock} />
                                    <div className={' px-3 mt-3'}>
                                        <p className='mb-0 hv-b pointer h5'> Hylet </p>
                                    </div>
                                </div>
                                <div className='mt-2'>
                                    <Button type='primary' onClick={showAcceptModal}>Accept Offer</Button>
                                    <span className='px-1'></span>
                                    <Button onClick={showRejectModal}>Reject</Button>
                                </div>
                            </div>
                            <div className="mt-4 text-secondary d-flex justify-content-between">
                                <span>
                                    Price : <strong>₹ 300</strong>
                                </span>
                                <span>
                                    Type : <strong>Story</strong>
                                </span>
                                <span>
                                    Days : <strong>1</strong>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mt-5">
                    <OfferGeneral />
                </div>
            </div>
            <Modal title="Reject Offer" visible={isRejectVisible} footer={null} onCancel={hideRejectModal}> 
                <p>Are you sure you want to reject this offer ?</p>

                <Form
                    name="rejectForm"
                    onFinish={onRejectFinish}
                    onFinishFailed={onRejectFinishFailed}
                    autoComplete="off"
                    layout='vertical'
                    >
                    <Form.Item
                        label="Reason (Optional)"
                        name="reason"
                    >
                        <Input.TextArea />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit">
                            Reject
                        </Button>
                        <span className='px-2'></span>
                        <Button onClick={hideRejectModal}>Cancel</Button>
                    </Form.Item>
                </Form>
            </Modal>
            <Modal title="Accept Offer" visible={isAcceptVisible} footer={null} onCancel={hideAcceptModal}> 
                <p>Are you sure you want to accept this offer ?</p>

                <Form
                    name="rejectForm"
                    onFinish={onRejectFinish}
                    onFinishFailed={onRejectFinishFailed}
                    autoComplete="off"
                    layout='vertical'
                    >
                    <Form.Item
                        label="Post/Story Link"
                        name="link"
                    >
                        <Input />
                    </Form.Item>
                    <div>
                        <small className="text-secondary">This will be verified by our people and amount will credited to your
                         account only after successful verification. So please make sure to use the correct link.</small>
                    </div>
                    <Form.Item className='mt-2'>
                        <Button type="primary" htmlType="submit">
                            Accept
                        </Button>
                        <span className='px-2'></span>
                        <Button onClick={hideAcceptModal}>Cancel</Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    )
}

export default OfferView;
