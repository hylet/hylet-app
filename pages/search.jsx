import React from 'react'
import SearchCard from '../components/Search/SearchCard';

const Search = ()=> {
    return (
        <div className='container-fluid'>
            <div className="row">
                <div className="col-24">
                    <div className="text-4">Showing 21 Results For  <strong className='text-b'>&apos;aicte&apos;</strong> </div>
                </div>
            </div>
            <div className="mt-4">
                <SearchCard />
                <SearchCard />
            </div>
        </div>
    )
}

export default Search;