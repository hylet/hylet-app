import Head from 'next/head'
import Home from '../components/Home/Home'
import Layout from "../components/Layout/Layout";

export default function App() {
  return (
    <div>
      <Head>
        <title>Hylet - Marketing Simplified</title>
        <meta name="Hylet" content="Marketing Simplified" />
      </Head>
        <Home />
    </div>
  )
}
