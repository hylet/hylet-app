import React,{useState} from 'react';
import Head from "next/head";
import {Button,Drawer } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import AdForm from '../../components/Ads/AdForm';
import AdList from '../../components/Ads/AdList';

const Adds = (props)=> {
    const [visible,setVisible] = useState(false);
    
    const showDrawer = () => {
        setVisible(true);    
    };
    
    const onClose = () => {
        setVisible(false)
    };
    return (
        <div className='px-2'>
            <Head>
                <title>Hylet - My Ads</title>
                <meta name="Hylet" content="Marketing Simplified" />
                <link rel="icon" href="/hylet_logo.png" />
            </Head>
            <div className='titlebar rounded'>
                <div className="d-flex justify-content-between">
                    <div>
                        <div className='h5'>My Ads</div>
                        <small className='text-secondary'>Create and manage all your ads</small> 
                    </div>    
                    <Button type='primary' onClick={showDrawer} icon={<PlusOutlined />}>New Ad</Button>
                </div>
            </div>
            <div className="mt-4">
                <AdList />
            </div>
            <Drawer
                title="Add new Page"
                width={"100%"}
                onClose={onClose}
                visible={visible}
                bodyStyle={{ paddingBottom: 80 }}
                >
                <div className='d-flex justify-content-between px-5'>
                    <div className="h5  mb-5">
                        Add new Page
                    </div>
                    <Button onClick={onClose}>Cancel</Button>
                </div>
                <div className="px-5">
                    <AdForm />    
                </div>
            </Drawer>
        </div>
    );
}

export default Adds;