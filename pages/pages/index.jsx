import React,{useState} from 'react';
import Head from "next/head";
import {Button,Drawer } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import PageCard from '../../components/Pages/PageCard';
import PageForm from '../../components/Pages/PageForm';

const Pages = (props)=> {
    const [visible,setVisible] = useState(false);
    
    const showDrawer = () => {
        setVisible(true);    
    };
    
    const onClose = () => {
        setVisible(false)
    };

    return (
        <div className='px-2'>
            <Head>
                <title>Hylet - My Pages</title>
                <meta name="Hylet" content="Marketing Simplified" />
                <link rel="icon" href="/hylet_logo.png" />
            </Head>
            <div className='titlebar rounded'>
                <div className="d-flex justify-content-between">
                    <div>
                        <div className='h5'>My Pages</div>
                        <small className='text-secondary'>List of your social media handles</small> 
                    </div>    
                    <Button type='primary' onClick={showDrawer} icon={<PlusOutlined />}>Add Page</Button>
                </div>
            </div>  
            <div className="mt-4">
                <div className="row">
                    <PageCard />
                    <PageCard />
                    <PageCard />
                </div>
            </div>
            <Drawer
                title="Add new Page"
                width={"100%"}
                onClose={onClose}
                visible={visible}
                bodyStyle={{ paddingBottom: 80 }}
                >
                <div className='d-flex justify-content-between px-5'>
                    <div className="h5  mb-5">
                        Add new Page
                    </div>
                    <Button onClick={onClose}>Cancel</Button>
                </div>
                <div className="px-5">
                    <PageForm />    
                </div>
            </Drawer>
        </div>
    );
}

export default Pages;