import React from 'react';
import Head from "next/head";
import TopPicks from '../components/Top/Toppicks';

const Top = (props)=> {
    return (
        <div className="px-2">
            <Head>
                <title>Hylet - Top Picks</title>
                <meta name="Hylet" content="Marketing Simplified" />
                <link rel="icon" href="/hylet_logo.png" />
            </Head>
            <div className='titlebar rounded'>
                <div className="d-flex justify-content-between">
                    <div>
                        <div className='h5'>Top Picks</div>
                        <small className='text-secondary'>List of Featured pages for you</small> 
                    </div>    
                </div>
            </div>  
            <TopPicks/>
        </div>
        
    );
}

export default Top;