import React,{useState} from 'react';
import Head from "next/head";
import Checkout from '../../../components/Payments/Checkout';
const CheckoutComponent = (props)=> {
    
    return (
        <div className='px-2'>
            <Head>
                <title>Hylet - My Payments</title>
                <meta name="Hylet" content="Marketing Simplified" />
                <link rel="icon" href="/hylet_logo.png" />
            </Head>
            <div className='titlebar rounded'>
                <div className="d-flex justify-content-between">
                    <div>
                        <div className='h5'>Checkout</div>
                        <small className='text-secondary'>Make payments for ads</small> 
                    </div>    
                </div>
            </div>
            <div className="mt-4">
                <Checkout />
            </div>
        </div>
    );
}

export default CheckoutComponent;