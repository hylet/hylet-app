import React from 'react';
import Head from "next/head";
import mock from '../../assets/img/mock.png'
import { Descriptions,Button } from 'antd';
import Image from "next/image"
import { MoreOutlined } from '@ant-design/icons';
const Settings = (props)=> {
    return (
        <div>
            <Head>
                <title>Hylet - Settings</title>
                <meta name="Hylet" content="Marketing Simplified" />
                <link rel="icon" href="/hylet_logo.png" />
            </Head>
            <div className='titlebar rounded'>
                <div className="d-flex justify-content-between">
                    <div>
                        <div className='h5'>Settings</div>
                    </div>    
                </div>
            </div>
            <div className="row  mt-2 d-flex justify-content-between">
                <div className="col-lg-7 col-12 mt-4 mx-sm-2 px-3 py-2 shadow-sm">
                    <div className="d-flex justify-content-between">
                        <div className='d-flex'>
                            <Image preview={"false"} className={'rounded w-100 rounded-circle'} height='75' width='75' alt='logo' src={mock} />
                            <span className="px-4 py-3">
                                Yash
                            </span>
                        </div>
                        <div>
                            <Button type='text' icon={<MoreOutlined />} />
                        </div>
                    </div>
                    <Descriptions bordered column={1} className='mt-3'>
                        <Descriptions.Item label="Email">yash@gmail.com</Descriptions.Item>
                        <Descriptions.Item label="Website">www.onerepublic.com</Descriptions.Item>
                        <Descriptions.Item label="Bio">oner4ever</Descriptions.Item>         
                    </Descriptions>
                </div>
                <div className="col-lg-4 col-12 mx-sm-2  mt-4 px-3 py-2 shadow-sm">
                    <div className='h6'>Payment Details</div>
                    <div className='text-secondary'>
                        Note : All the payments and refunds will be sent to the below mentioned account  
                    </div>
                    <div className="mt-3">
                    <Descriptions bordered column={1} className='mt-3'>
                        <Descriptions.Item label="Payment Type">Gpay</Descriptions.Item>
                        <Descriptions.Item label="Payment Number">9909898789</Descriptions.Item>       
                    </Descriptions>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Settings;