import React,{useState} from 'react'
import { Form, Input, Button, Checkbox ,Select,Image,Upload } from 'antd';
import {LoadingOutlined,PlusOutlined} from '@ant-design/icons';
const { Option } = Select;

const PageForm = ()=> {
      const [imgUrl, setImgUrl] = useState('');
      const [imgUploadLoading, setImgUploadLoading] = useState(false);

      const onFinish = () => {
        console.log('Success:', values);
      };
    
      const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
      };

      const uploadButton = (<div>{imgUploadLoading ? <LoadingOutlined /> : <PlusOutlined />}<div style={{ marginTop: 8 }}>Profile Pic</div></div>);
      
      const getBase64 = (img, callback)=> {
          const reader = new FileReader();
          reader.addEventListener('load', () => callback(reader.result));
          reader.readAsDataURL(img);
      }
      
      const beforeUpload = (file)=> {
          const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
          if (!isJpgOrPng) {
              message.error('You can only upload JPG/PNG file!');
          }
          const isLt2M = file.size / 1024 / 1024 < 2;
          if (!isLt2M) {
              message.error('Image must smaller than 2MB!');
          }
          return isJpgOrPng && isLt2M;
      }
    
    
      const handleChange = info => {
        if (info.file.status === 'uploading') {
            setImgUploadLoading(true)
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            getBase64(info.file.originFileObj, imageUrl => {
                    setImgUploadLoading(false)
                    setImgUrl(imageUrl)
                console.log(imageUrl)
                }
            );
        }
    };

    
    return (
  <Form
        name="basic"
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
      <div className='text-center'>
          <Upload
          name="avatar"
          listType="picture-card"
          className="avatar-uploader"
          showUploadList={false}
          howUploadList={false}
          beforeUpload={beforeUpload}
          onChange={handleChange}
          style={{borderRadius:'50%'}}
        >
          {imgUrl ? <Image preview={false} src={imgUrl} alt="avatar" style={{width: '100%'}}/> : uploadButton}
        </Upload>

      </div>
      <Form.Item
        label="Display Name"
        name="displayName"
        rules={[
          {
            required: true,
            message: 'Please input your display name!',
          },
        ]}
      >
        <Input placeholder='Your display name'/>
      </Form.Item>

      <Form.Item
        label="Page Name"
        name="pageName"
        rules={[
          {
            required: true,
            message: 'Please input your page name!',
          },
        ]}
      >
        <Input    placeholder='Your social media handle name'/>
      </Form.Item>

      <Form.Item name="accType" label="Account Type" rules={[{ required: true, message: 'Please input your account type!', }]}>
        <Select
          placeholder="Select your account type"
          allowClear
        >
          <Option value="instagram">Instagram</Option>
          <Option value="youtube">Youtube</Option>
          <Option value="linkedin">LinkedIn</Option>
        </Select>
      </Form.Item>

      <Form.Item name="followers" label="Followers / Subscribers" rules={[{ required: true, message: 'Please input your followers count!', }]}>
        <Select
          placeholder="Select number of followers"
          allowClear
        >
          <Option value="1">1K +</Option>
          <Option value="10">10K +</Option>
          <Option value="50">50K +</Option>
          <Option value="100">100K +</Option>
          <Option value="250">250K +</Option>
          <Option value="500">500K +</Option>
          <Option value="750">750K +</Option>
          <Option value="1000">1M +</Option>
          <Option value="3000">3M +</Option>
        </Select>
      </Form.Item>  
      
      <Form.Item name="pageType" label="Page Type" rules={[{ required: true, message: 'Please input your page type!', }]}>
        <Select
          placeholder="Select one which best suits your handle"
          allowClear
        >
          <Option value="meme">Meme</Option>
          <Option value="infulencer">Infulencer</Option>
          <Option value="informational">Informational</Option>
          <Option value="education">School / College</Option>
        </Select>
      </Form.Item>

      <Form.Item
        name="remember"
        valuePropName="checked"
      >
        <Checkbox>I have provided truthful information and would like to apply for page verification</Checkbox>
      </Form.Item>

      <Form.Item
      >
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
    )
}

export default PageForm;
