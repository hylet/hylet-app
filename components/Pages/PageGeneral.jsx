import React from 'react'
import { Descriptions, Badge } from 'antd';
const PageGeneral = ()=> {
    return (
        <div>
            <div className="d-flex justify-content-between mb-3">
                <div className="h5">General</div>
            </div>
            <Descriptions layout="vertical" bordered>
                <Descriptions.Item label="Display Name">Mokka Engineer</Descriptions.Item>
                <Descriptions.Item label="Handle Name">mokka_engineer</Descriptions.Item>
                <Descriptions.Item label="Status" span={3}>
                    <Badge status="processing" text="live" />
                </Descriptions.Item>
                <Descriptions.Item label="Page type">Meme</Descriptions.Item>
                <Descriptions.Item label="Followers">1M +</Descriptions.Item>
                <Descriptions.Item label="Verified">YES</Descriptions.Item>
                <Descriptions.Item label="Created at">2018-04-24 18:00:00</Descriptions.Item>
                <Descriptions.Item label="Desc" span={2}>
                    Fun filled entertainment page
                </Descriptions.Item>
                
            </Descriptions>
        </div>
    )
}

export default  PageGeneral;