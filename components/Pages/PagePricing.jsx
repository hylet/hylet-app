import React from 'react'
import { Table ,Button} from 'antd';
import { EditOutlined,DeleteOutlined } from '@ant-design/icons';

export default function Pricing() {
    const columns = [
        {
            title: 'Type',
            dataIndex: 'type',
            render: (text) => <a>{text}</a>,
        },
        {
            title: 'Price',
            dataIndex: 'price',
        },
        {
            title: 'Day(s)',
            dataIndex: 'days',
        },
        {
            title: 'Operation',
            dataIndex: 'operation',
            render: () =>{
                return (<>
                    <Button type='text'  icon={<EditOutlined />} />
                    <Button type='text' className='mx-3'  icon={<DeleteOutlined />} />
                </>); 
            }
          },
    ];

    const data = [
        {
          key: '1',
          type: 'Post',
          days: 1,
          price: "₹ 1000",
        },
        {
            key: '2',
            type: 'Story',
            days: 1,
            price: "₹ 1000",
        },
        {
            key: '3',
            type: 'Story + Post',
            days: 1,
            price: "₹ 1500",
        },
    ]

    return (
        <div>
            <div className="d-flex justify-content-between mb-3">
                <div className="h5">Pricing</div>
                <div> <Button type='primary'>Add New</Button></div>
            </div>
                    <Table
                        pagination={false}
                        columns={columns}
                        dataSource={data}
                    />
        </div>
    )
}
