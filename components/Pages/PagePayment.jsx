import React from 'react'
import { Statistic,Table} from 'antd'

export default function PagePayment() {
    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            render: (text) => <a>{text}</a>,
        },
        {
            title: 'Date',
            dataIndex: 'date',
        },
        {
            title: 'Amount',
            dataIndex: 'amount',
        }
    ];

    const data = [
        {
          key: '1',
          name: 'Hylet',
          date: "12/2/22",
          amount: "₹ 1000",
        },
        {
            key: '2',
            name: 'CyberG',
            date: "03/2/22",
            amount: "₹ 3000",
          },

    ]
    return (
         <div>
            <div className="d-flex mb-3">
                <div className="h5">Payments</div>
            </div>
            <div className="d-flex justify-content-around">
                <Statistic title="Total Income" valueStyle={{ color: '#3f8600' }} value={4000} prefix="₹" />
                <Statistic title="Outstanding" value={1200} prefix="₹" />
            </div>
            <div className="mt-4">
            <div className="h5 mb-3">Statements</div>
                    <Table
                        pagination={false}
                        columns={columns}
                        dataSource={data}
                    />
            </div>
        </div>
    )
}
