import React from 'react'
import AdCard from '../Ads/AdCard'
export default function Pricing() {
   
    return (
        <div>
            <div className="d-flex justify-content-between mb-3">
                <div className="h5">Ads</div>
            </div>
            <div className="row">
                <div className="col-6 col-lg-3">
                    <AdCard />
                </div>
                <div className="col-6 col-lg-3">
                    <AdCard />
                </div>
                <div className="col-6 col-lg-3">
                    <AdCard />
                </div>
            </div>
        </div>
    )
}
