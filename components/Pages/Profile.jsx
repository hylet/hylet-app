import React,{useState} from 'react'
import Head from "next/head";
import { Tabs,Button,Menu, Dropdown,Drawer } from 'antd';
import { CheckCircleTwoTone,EditOutlined,MoreOutlined  } from '@ant-design/icons';
import Image from 'next/image';
import mock from '../../assets/img/mock.png'
import Pricing from './PagePricing';
import PagePayment from './PagePayment';
import PageAds from './PageAds';
import PageForm from './PageForm';
import PageGeneral from './PageGeneral';
const { TabPane } = Tabs;

const Profile = ()=> {
    
    const [visible,setVisible] = useState(false);
    
    const showDrawer = () => {
        setVisible(true);    
    };
    
    const onClose = () => {
        setVisible(false)
    };

    const mainMenu = (
        <Menu>
          <Menu.Item>
            <Button type='text' onClick={showDrawer} icon={<EditOutlined />}>Edit</Button>
          </Menu.Item>
          <Menu.Item>
            <Button type='text'>Make Inactive</Button>
          </Menu.Item>
        </Menu>
      );
      
    return (
        <div className='px-2 '>
            <Head>
                <title>Hylet - Kiddo</title>
                <meta name="Hylet" content="Marketing Simplified" />
                <link rel="icon" href="/hylet_logo.png" />
            </Head>
            <div className='titlebar rounded'>
                <div className="row ">
                    <div className="col-lg-2 col-3">
                        <Image preview={"false"} className={'rounded rounded-circle'} height='150' width='150'  alt='logo' src={mock} />
                    </div>
                    <div className="col-lg-4 col-9">
                        <div className={' px-3 mt-3'}>
                            <div className="d-flex justify-content-between">
                                <p className='mb-0 h5'> Mokka Engineer <span><CheckCircleTwoTone twoToneColor="#52c41a" /></span></p>
                                <Dropdown overlay={mainMenu} placement="bottomLeft" arrow>
                                    <Button type='text' icon={<MoreOutlined />} />
                                </Dropdown>
                            </div>
                            <small className='text-secondary'> active <span className='px-2 text-b'>|</span> visit page </small>
                            <div className="mt-3 d-flex justify-content-between">
                                <div>
                                    <span className='text-o'>1M </span>
                                    <span className='ml-3'>followers</span>
                                </div>
                                <div>
                                    <span className='text-o'>17 </span>
                                    <span className='ml-3'>ads</span>
                                </div>
                                <div>
                                    <span className='text-o'>2 </span>
                                    <span className='ml-3'>live</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mt-4">
                <Tabs defaultActiveKey="1" size='large'  style={{ marginBottom: 32}}>
                <TabPane tab="General" key="1">
                    <PageGeneral />
                </TabPane>
                <TabPane tab="Pricing" key="2">
                    <Pricing />
                </TabPane>
                <TabPane tab="Ads" key="3">
                    <PageAds />
                </TabPane>
                <TabPane tab="Payments" key="4">
                    <PagePayment />
                </TabPane>
                </Tabs>
                </div>
            </div>
            <Drawer
                title="Add new Page"
                width={"100%"}
                onClose={onClose}
                visible={visible}
                bodyStyle={{ paddingBottom: 80 }}
                >
                <div className='d-flex justify-content-between px-5'>
                    <div className="h5 mb-5">
                        Edit Page Details
                    </div>
                    <Button onClick={onClose}>Cancel</Button>
                </div>
                <div className="px-5">
                    <PageForm />    
                </div>
            </Drawer>  
        </div>
    )
}

export default Profile;
