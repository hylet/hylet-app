import React from 'react';
import Head from "next/head";
import { Table } from 'antd';
import { CheckCircleTwoTone } from '@ant-design/icons';
import Image from 'next/image';
import mock from '../../assets/img/mock.png'
import BrandCard from '../../components/Home/BrandCard';

const PageView = ()=> {
    const columns = [
        {
            title: 'Type',
            dataIndex: 'type',
            render: (text) => <a>{text}</a>,
        },
        {
            title: 'Price',
            dataIndex: 'price',
        },
        {
            title: 'Day(s)',
            dataIndex: 'days',
        },
        
    ];

    const data = [
        {
          key: '1',
          type: 'Post',
          days: 1,
          price: "₹ 1000",
        },
        {
            key: '2',
            type: 'Story',
            days: 1,
            price: "₹ 1000",
        },
        {
            key: '3',
            type: 'Story + Post',
            days: 1,
            price: "₹ 1500",
        },
    ]

    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
          console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        getCheckboxProps: (record) => ({
          disabled: record.name === 'Disabled User',
          // Column configuration not to be checked
          name: record.name,
        }),
      };

    return (
        <div className='px-2 '>
            <Head>
                <title>Hylet - Kiddo</title>
                <meta name="Hylet" content="Marketing Simplified" />
                <link rel="icon" href="/hylet_logo.png" />
            </Head>
            <div className='titlebar rounded'>
                <div className="row ">
                    <div className="col-lg-2 col-3">
                        <Image preview={"false"} className={'rounded rounded-circle'} height='150' width='150'  alt='logo' src={mock} />
                    </div>
                    <div className="col-lg-4 col-9">
                        <div className={' px-3 mt-3'}>
                            <p className='mb-0 h5'> Mokka Engineer <span><CheckCircleTwoTone twoToneColor="#52c41a" /></span></p>
                            <small className='text-secondary'> active <span className='px-2 text-b'>|</span> visit page </small>
                            <div className="mt-3 d-flex justify-content-between">
                                <div>
                                    <span className='text-o'>1M </span>
                                    <span className='ml-3'>followers</span>
                                </div>
                                <div>
                                    <span className='text-o'>17 </span>
                                    <span className='ml-3'>ads</span>
                                </div>
                                <div>
                                    <span className='text-o'>2 </span>
                                    <span className='ml-3'>live</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mt-5">
                    <div className='h5'>Pricing</div>
                    <Table
                        rowSelection={{
                        type: 'radio',
                        ...rowSelection,
                    }}
                        pagination={false}
                        columns={columns}
                        dataSource={data}
                    />
                </div>
                <div className="mt-4">
                    <div className="row">
                        <div className="col-lg-3 col-12">
                            <BrandCard />
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    )
}

export default PageView;