import React from 'react';
import Image from 'next/image';
import mock from '../../assets/img/mock.png'
import { CheckCircleTwoTone } from '@ant-design/icons';
import Link from 'next/link';

const PageCard = ()=> {
    return (
        <div className="col-12 col-lg-4">
            <div className="mt-3 card">
                <div className="d-flex justify-content-start card-header">
                    <Image preview={"false"} className={'rounded w-100 rounded-circle'} height='50' width='50' alt='logo' src={mock} />
                    <Link href='/pages/kiddo' passHref>
                        <div className={' px-3 mt-3'}>
                            <p className='mb-0 hv-b pointer'> Mokka Engineer <span><CheckCircleTwoTone twoToneColor="#52c41a" /></span></p>
                            <small className='text-secondary'>active </small>
                        </div>
                    </Link>
                </div>
                <div className="card-body text-secondary">
                    <div className="d-flex justify-content-between">
                        <p>Total Ads : <strong>13</strong></p>
                        <p>Live : <strong>2</strong></p>
                    </div>
                    Total Income : <strong>₹ 30000</strong>
                </div>
            </div>
        </div>
    );
}

export default PageCard;