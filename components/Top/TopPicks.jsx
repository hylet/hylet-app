import React from 'react';
import {  Button, Carousel, Input} from 'antd';
import BrandCard from "./BrandCard";
import ad1 from '../../assets/img/ad1.png';
import ad2 from "../../assets/img/ad2.png";
import ad3 from '../../assets/img/ad3.png';
import {CheckOutlined, VideoCameraOutlined} from "@ant-design/icons";
import Image from "next/image";


const TopPicks = (props)=> {

    return (
        
        <div className="container my-3 text-secondary">
            <div className=" mt-5">
                <h3 className={'text-secondary'}>Featured</h3>
            </div>
            <div className="d-flex flex-row justify-content-between mt-3 mb-1">
                <div>
                    <p className={'text-secondary'}>Most Followers </p>
                </div>
                <div className={'text-b'}>More</div>
            </div>
            <div className="row">
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
            </div>
            <div className="d-flex flex-row justify-content-between mt-5 mb-1">
                <div>
                    <p className={'text-secondary'}>Most Ads </p>
                </div>
                <div className={'text-b'}>More</div>
            </div>
            <div className="row">
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
            </div>
        </div>
    );
}


export default TopPicks;