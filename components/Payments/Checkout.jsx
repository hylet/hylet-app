import React from 'react'
import { Table ,Button} from 'antd';

export default function Checkout() {
    const columns = [
        {
            title: 'S.no',
            dataIndex: 'sNo',
        },
        {
            title: 'Name',
            dataIndex: 'pageName',
        },
        {
            title: 'Type',
            dataIndex: 'adType',
        },
        {
            title: 'Price',
            dataIndex: 'price',
          },
          
    ];

    const data = [
        {
          key: '1',
          sNo: '1',
          pageName: "Be like Bruh",
          adType:"Story",
          price: 1000,
        },
        {
            key: '2',
            sNo: '3',
            pageName: "Be like Macha",
            adType:"Post",
            price: 1000,
          },

    ]

    return (
        <div>
            <div className="d-flex justify-content-between mb-3">
                <div className="h6">New Launch</div>
            </div>
           <Table
                pagination={false}
                columns={columns}
                dataSource={data}
                summary={pageData => {
                    let sum = 0;

                    pageData.forEach(({ price }) => {
                      sum += price;
                    });
            
                    return (
                      <>
                        <Table.Summary.Row>
                          <Table.Summary.Cell colSpan={2}></Table.Summary.Cell>
                          <Table.Summary.Cell>Sub total</Table.Summary.Cell>
                          <Table.Summary.Cell>
                            {sum}
                          </Table.Summary.Cell>
                        </Table.Summary.Row>
                        <Table.Summary.Row>
                          <Table.Summary.Cell colSpan={2}></Table.Summary.Cell>
                          <Table.Summary.Cell className='text-o'>Total</Table.Summary.Cell>
                          <Table.Summary.Cell className='text-o'>
                            {sum+10}
                          </Table.Summary.Cell>
                        </Table.Summary.Row>
                        <Table.Summary.Row>
                          <Table.Summary.Cell colSpan={3}></Table.Summary.Cell>
                          <Table.Summary.Cell>
                            <Button type='primary' danger>Make Payment</Button>
                          </Table.Summary.Cell>
                        </Table.Summary.Row>
                      </>
                    );
                  }}
            />
        </div>
    )
}
