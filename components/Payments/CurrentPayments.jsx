import React from 'react'
import { Table ,Button} from 'antd';
import Link from 'next/link';


const  CurrentPayments = ()=> {
    const columns = [
        {
            title: 'Date',
            dataIndex: 'date',
        },
        {
            title: 'Ad',
            dataIndex: 'adName',
        },
        {
            title: 'Pages',
            dataIndex: 'pages',
        },
        {
            title: 'Price',
            dataIndex: 'price',
            
        },
        {
            title:'Payment',
            dataIndex:'payment',
            render: () =>{
                return (
                    <Link href='/payments/1234' passHref>
                        <Button type='primary' danger>Checkout</Button>
                    </Link>
                )
               
            }
        }
    ];

    const data = [
        {
          key: '1',
          date: '22/12/21',
          adName: "My Ad",
          pages: "3",
          price:"₹ 1000",
        }
    ]

    return (
        <div>
            <div className="d-flex justify-content-between mb-3">
                <div className="h6">Payment Dues</div>
            </div>
                    <Table
                        pagination={false}
                        columns={columns}
                        dataSource={data}
                    />
        </div>
    )
}

export default CurrentPayments;