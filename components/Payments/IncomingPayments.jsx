import React from 'react'
import { Table ,Button,Badge} from 'antd';


const  IncomingPayments = ()=> {
    const columns = [
        {
            title: 'Date',
            dataIndex: 'date',
        },
        {
            title: 'Vendor',
            dataIndex: 'vendorName',
        },
        {
            title: 'Amount',
            dataIndex: 'amount',
        },
        {
            title: 'Txn',
            dataIndex: 'txn',
            render:()=>{
                return (
                    <>
                        <Badge count="- Deb" />   
                    </>
                )
            }
        }
    ];

    const data = [
        {
          key: '1',
          date: '22/12/21',
          vendorName: "Be like Bruh",
          amount: "₹ 300",
          txn:'Cred'
        }
    ]

    return (
        <div>
            <div className="d-flex justify-content-between mb-3">
                <div className="h6">History</div>
            </div>
                    <Table
                        pagination={false}
                        columns={columns}
                        dataSource={data}
                    />
        </div>
    )
}

export default IncomingPayments;