import React from 'react'
import {Form, Input, Button, Select, Space} from 'antd';
import {KeyOutlined, MailOutlined, UserOutlined} from "@ant-design/icons";

const { Option } = Select;

const SignUp = (props)=> {
    
    const {toggleLogin} = props;

    const onFinish = (values) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo) => {
        // console.log('Failed:', errorInfo);
    };
    
    return (
        <div>
            <p className={'text-secondary pb-2 mt-4 text-start'}>Hi there! Let&apos;s get you Started 😄</p>
            <Form
                name="signup"
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
                className='text-start'
            >

                    <Form.Item
                        name="username"
                        rules={[{ required: true, message: 'Please enter your username!' }]}
                    >
                        <Input size={'large'} prefix={<UserOutlined />} placeholder="Username" />
                    </Form.Item>

                    <Form.Item
                        name="email"
                        rules={[{ required: true, message: 'Please enter your email!' },{type:'email',message:"Please enter a valid email"}]}
                    >
                        <Input size={'large'} prefix={<MailOutlined />} placeholder="Email" />
                    </Form.Item>

                    <Form.Item name="type" rules={[{ required: true }]}>
                        <Select
                        placeholder="I am a "
                        allowClear
                        size='large'
                        prefix={<UserOutlined />}
                        >
                        <Option value="owner">Business Owner</Option>
                        <Option value="creator">Content Creator</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item

                        name="password"
                        rules={[{ required: true, message: 'Please input your password!' },{min: 6, message: 'Password must be atleast 6 characters' }]}
                    >
                        <Input.Password size={'large'}  prefix={<KeyOutlined />} placeholder="Password"/>
                    </Form.Item>

                    <Form.Item
                        name="confirm"
                        dependencies={['password']}
                        hasFeedback
                        rules={[
                        {
                            required: true,
                            message: 'Please confirm your password!',
                        },
                        ({ getFieldValue }) => ({
                            validator(_, value) {
                            if (!value || getFieldValue('password') === value) {
                                return Promise.resolve();
                            }

                            return Promise.reject(new Error('The two passwords that you entered do not match!'));
                            },
                        }),
                        ]}
                    >
                        <Input.Password  prefix={<KeyOutlined />} placeholder="Confirm Password" size={'large'}/>
                    </Form.Item>
                    
                    <Form.Item className='pt-3'>
                            <Button block={true} type="primary" htmlType="submit" size='large'>
                                Sign Up
                            </Button>
                    </Form.Item>
            </Form>

            <div className=" text-center">
                <div>or</div>
                <div className='mt-2 mb-3'>
                    Already have an account ? <Button type='link' onClick={toggleLogin}>Sign in</Button>
                </div>
            </div>
        </div>
    )
}

export default SignUp;


