import React from 'react'
import {Form, Input, Button} from 'antd';
import {KeyOutlined, MailOutlined} from "@ant-design/icons";

const SignIn = (props)=> {

    const {toggleLogin} = props;
    
    const onFinish = (values) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo) => {
        // console.log('Failed:', errorInfo);
    };

    return (
        <div>
            <p className={'text-secondary pb-2 mt-4 text-start'}>Welcome back ! Please login to your account.</p>
            <Form
                name="signin"
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                    <Form.Item
                        name="username"
                        rules={[{ required: true, message: 'Please enter your email!' },{type:'email',message:"Please enter a valid email"}]}
                    >
                        <Input size={'large'} prefix={<MailOutlined />} placeholder="Email" />
                    </Form.Item>

                    <Form.Item

                        name="password"
                        rules={[{ required: true, message: 'Please enter your password!' },{min: 6, message: 'Password must be atleast 6 characters' }]}
                    >
                        <Input.Password size={'large'}  prefix={<KeyOutlined />} placeholder="Password"/>
                    </Form.Item>
                    <div className="mt-3 text-end text-secondary">
                        Forgot Password ?
                    </div>

                    <Form.Item className='pt-3'>
                            <Button block={true} type="primary" htmlType="submit" size='large'>
                                Sign In
                            </Button>
                    </Form.Item>
            </Form>
            <div className="mt-4 text-center">
                <div>or</div>
                <div className='mt-3'>
                    Don&apos;t have an account ? <Button type='link' onClick={toggleLogin}>Sign Up</Button>
                </div>
            </div>
        </div>
    )
}

export default SignIn;


