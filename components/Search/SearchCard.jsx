import React from 'react'
import Image from 'next/image'
import {Button} from 'antd'
import {StarFilled,CheckCircleTwoTone} from '@ant-design/icons';
import mock from '../../assets/img/mock.png';
import Link from 'next/link';


const SearchCard = ()=> {
    return (
        <div className="row mt-3">
            <div className="col-6 col-lg-2">
                <Image alt='img' className={'rounded'} height='200' width='200' src={mock} />
            </div>
            <div className="col-6 col-lg-8">
                <div className="h6 mt-2">Be Like Bro <span><CheckCircleTwoTone className='mb-2' twoToneColor="#52c41a" /></span></div>
                <div>
                    <small className='text-secondary'>
                        750K<span className='px-3'>|</span>Insta
                    </small>
                </div>
                <StarFilled className="text-o" /><StarFilled className="text-o" /><StarFilled className="text-o" /><StarFilled className="text-o" />
                <div className="mt-4">
                    <div >Post + Story : <span className='text-o'>₹ 4000</span></div>
                    <div className='mt-2 text-primary'>
                        <Link passHref href="#">
                            See more offerings
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SearchCard;