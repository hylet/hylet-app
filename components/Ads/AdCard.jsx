import React from "react";
import { Image } from 'antd';
const AdCard = ()=>{
    return (
        <>
           <div className="mt-3">
                <Image  className={'rounded'} width={'100%'} height={140} alt='logo' src={'https://i.insider.com/5bbd187101145529745a9895?width=700'} />
                <div className={'mt-2'}>
                    <div className="d-flex justify-content-between">
                        <div className='d-flex justify-content-start'>
                            <Image preview={"false"} className={'rounded w-100 rounded-circle'} height={50 }width={50} alt='logo' src={'https://res.cloudinary.com/crunchbase-production/image/upload/c_lpad,f_auto,q_auto:eco,dpr_1/v1488266135/vw9dwa2fbgqzdmkizszx.png'} />
                            <p className={'text-d-s px-2 mt-3'}> Burger King</p>
                        </div>
                    </div>
                </div>
            </div>        
        </>
    )
}

export default AdCard;