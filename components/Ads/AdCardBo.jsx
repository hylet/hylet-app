import React from "react";
import { Image,Badge } from 'antd';
import { ShoppingCartOutlined } from '@ant-design/icons';
import Link from 'next/link';

const AdCard = ()=>{
    return (
        <>
           <div className="mt-4" text='Need Payment' color='yellow'>
                <Image preview={false}  className={'rounded'} width={'100%'} height={140} alt='logo' src={'https://i.insider.com/5bbd187101145529745a9895?width=700'} />
                <div className={'mt-2'}>
                    <div className="d-flex justify-content-between">
                        <div>
                            <Link href='/adds/1212123123' passHref>
                                <p className="mb-0 hv-b pointer h6"> New launch</p>
                            </Link>
                            <small className='text-secondary mt-2'>12/12/12</small>
                        </div>
                        <Badge title='Payment Pending' count={"₹ pending"} />
                    </div>
                    <div className="d-flex justify-content-between mt-2">
                        <div className='h6'><span className='text-secondary'>Cart <ShoppingCartOutlined /> :</span>  7</div>
                        <div className='h6'><span className='text-secondary'><Badge status="processing" /></span>3 <span className='text-secondary h6'>live</span></div>
                    </div>
                </div>
            </div>        
        </>
    )
}

export default AdCard;