import React from 'react'
import { Statistic,Table, Badge} from 'antd'

const AdCart = ()=> {
    const columns = [
        {
            title: 'Page Name',
            dataIndex: 'pageName',
            render: (text) => <a>{text}</a>,
        },
        {
            title: 'Promotion Type',
            dataIndex: 'type',
        },
        {
            title: 'Price ₹ ',
            dataIndex: 'price',
        },
        {
            title: 'Paid',
            dataIndex: 'paymentStatus',
        },
        {
            title: 'Status',
            dataIndex: 'promoStatus',
            render: (text) => <Badge title='Pending' count={text} />,
        }
    ];

    const data = [
        {
          key: '1',
          pageName: 'Hylet',
          type: "Story",
          price: "1000",
          paymentStatus: "Yes",
          promoStatus: "Pending",
        }

    ]
    return (
         <div>
            <div className="d-flex mb-3">
                <div className="h5">Cart</div>
            </div>
            <div className="d-flex justify-content-around">
                <Statistic title="Total Cost" valueStyle={{ color: '#3f8600' }} value={4000} prefix="₹" />
                <Statistic title="Yet to pay" value={1200} prefix="₹" />
            </div>
            <div className="mt-4">
            <div className="h5 mb-3">Details</div>
                    <Table
                        pagination={false}
                        columns={columns}
                        dataSource={data}
                    />
            </div>
        </div>
    )
}

export default AdCart;