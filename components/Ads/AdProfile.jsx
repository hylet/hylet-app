import React,{useState} from 'react'
import Head from "next/head";
import { Tabs,Button,Menu, Dropdown,Badge } from 'antd';
import { MoreOutlined  } from '@ant-design/icons';
import Image from 'next/image';
import mock from '../../assets/img/mock.png'
import AdGeneral from './AdGeneral';
import AdCart from './AdCart';
import AdPromotions from './AdPromotions';
const { TabPane } = Tabs;

const AdProfile = ()=> {
    
    const [visible,setVisible] = useState(false);
    
    const showDrawer = () => {
        setVisible(true);    
    };
    
    const onClose = () => {
        setVisible(false)
    };

    const mainMenu = (
        <Menu>
          <Menu.Item>
            <Button type='text'>Delete</Button>
          </Menu.Item>
        </Menu>
      );
      
    return (
        <div className='px-2 '>
            <Head>
                <title>Hylet - New Launch</title>
                <meta name="Hylet" content="Marketing Simplified" />
                <link rel="icon" href="/hylet_logo.png" />
            </Head>
            <div className='titlebar rounded'>
                <div className="row ">
                    <div className="col-lg-2 col-3">
                        <Image preview={"false"} className={'rounded'} height='150' width='150'  alt='logo' src={mock} />
                    </div>
                    <div className="col-lg-4 col-9">
                        <div className={' px-3 mt-3'}>
                            <div className="d-flex justify-content-between">
                                <p className='mb-0 h5'> New Launch </p>
                                <Dropdown overlay={mainMenu} placement="bottomLeft" arrow>
                                    <Button type='text' icon={<MoreOutlined />} />
                                </Dropdown>
                            </div>
                            <small className='text-secondary'> <Badge title='Payment Pending' count={"₹ pending"} /> </small>
                            <div className="mt-3 d-flex justify-content-between">
                                <div>
                                    <span className='ml-3'>Cart</span>
                                    <span className='text-o'> 7 </span>
                                </div>
                                <div>
                                    <span className='ml-3'>Accepted</span>
                                    <span className='text-o'> 4 </span>
                                </div>
                                <div>
                                    <span className='ml-3'>Rejected</span>
                                    <span className='text-o'> 2 </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mt-4">
                <Tabs defaultActiveKey="1" size='large'  style={{ marginBottom: 32}}>
                    <TabPane tab="General" key="1">
                        <AdGeneral />
                    </TabPane>
                    <TabPane tab="Cart" key="2">
                        <AdCart />
                    </TabPane>
                    <TabPane tab="Promoted" key="3">
                        <AdPromotions />
                    </TabPane>
                </Tabs>
                </div>
            </div>
        </div>
    )
}

export default AdProfile;
