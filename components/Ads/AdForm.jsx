import React,{useState} from 'react'
import { Form, Input, Button,Image,Upload } from 'antd';
import {LoadingOutlined,PlusOutlined} from '@ant-design/icons';


const AdForm = ()=> {
      const [imgUrl, setImgUrl] = useState('');
      const [imgUploadLoading, setImgUploadLoading] = useState(false);

      const onFinish = () => {
        console.log('Success:', values);
      };
    
      const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
      };

      const uploadButton = (<div>{imgUploadLoading ? <LoadingOutlined /> : <PlusOutlined />}<div style={{ marginTop: 8 }}>Ad Poster</div></div>);
      
      const getBase64 = (img, callback)=> {
          const reader = new FileReader();
          reader.addEventListener('load', () => callback(reader.result));
          reader.readAsDataURL(img);
      }
      
      const beforeUpload = (file)=> {
          const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
          if (!isJpgOrPng) {
              message.error('You can only upload JPG/PNG file!');
          }
          const isLt2M = file.size / 1024 / 1024 < 2;
          if (!isLt2M) {
              message.error('Image must smaller than 2MB!');
          }
          return isJpgOrPng && isLt2M;
      }
    
    
      const handleChange = info => {
        if (info.file.status === 'uploading') {
            setImgUploadLoading(true)
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            getBase64(info.file.originFileObj, imageUrl => {
                    setImgUploadLoading(false)
                    setImgUrl(imageUrl)
                console.log(imageUrl)
                }
            );
        }
    };

    
    return (
  <Form
        name="basic"
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
      
      <Form.Item
        label="Ad Name"
        name="adName"
        rules={[
          {
            required: true,
            message: 'Please input your ad name!',
          },
        ]}
      >
        <Input placeholder='Your Ad name'/>
      </Form.Item>

      <Form.Item
        label="Ad Description"
        name="adDesc"
        rules={[
          {
            required: true,
            message: 'Please input your ad description!',
          },
        ]}
      >
        <Input.TextArea    placeholder='Describe what you do in few words'/>
      </Form.Item>

      <Form.Item
        label="Post Caption"
        name="adCaption"
        rules={[
          {
            required: true,
            message: 'Please input your ad caption!',
          },
        ]}
      >
        <Input    placeholder='Caption to be used during promotion'/>
      </Form.Item>

      <Form.Item
        label="Website Link (if any)"
        name="adWebsite"
      >
        <Input    placeholder='link to your website / app'/>
      </Form.Item>

      <Form.Item
        label="Profile Link (if any)"
        name="adProfile"
      >
        <Input  placeholder='link to your social media handle'/>
      </Form.Item>

      <div className='text-center'>
          <Upload
          name="avatar"
          listType="picture-card"
          className="avatar-uploader"
          showUploadList={false}
          howUploadList={false}
          beforeUpload={beforeUpload}
          onChange={handleChange}
          style={{borderRadius:'50%'}}
        >
          {imgUrl ? <Image preview={false} src={imgUrl} alt="avatar" style={{width: '100%'}}/> : uploadButton}
        </Upload>

      </div>

      <Form.Item
      >
          <div className='d-flex justify-content-around mt-5'>
            <Button  className='bg-y text-w px-5'>
                Preview 
            </Button>
            <Button  type="primary" htmlType="submit" className='px-5'>
                Create Ad
            </Button>
          </div>

      </Form.Item>
    </Form>
    )
}

export default AdForm;
