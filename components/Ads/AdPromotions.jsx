import React from 'react'
import { Table, Statistic} from 'antd'
const AdPromotions = ()=> {
    const columns = [
        {
            title: 'Page Name',
            dataIndex: 'pageName',
            render: (text) => <a>{text}</a>,
        },
        {
            title: 'Price ₹',
            dataIndex: 'price',
        },
        {
            title: 'Posted At',
            dataIndex: 'posted',
        },
        {
            title: 'Post',
            dataIndex: 'postLink',
            render: (text) => <a className='text-primary'>{text}</a>,
        }
    ];

    const data = [
        {
          key: '1',
          pageName: 'Be like Bruh',
          price: "300",
          posted:"4:05 PM 20/11/2021",
          postLink: "View Story",
        }
    ]

    return (
         <div>
            <div className="d-flex mb-3">
                <div className="h5">Promoted</div>
            </div>
            <div className="d-flex justify-content-around">
                <Statistic title="Amount Paid" value={4000} prefix="₹" />
                <Statistic title="Amount Used" value={300} prefix="₹" />
            </div>
            <div className="mt-4">
                    <Table
                        pagination={false}
                        columns={columns}
                        dataSource={data}
                    />
            </div>
        </div>
    )
}

export default AdPromotions;