import React,{useEffect} from 'react';
import { Input,Button, Menu, Dropdown,} from 'antd';
import logo from '../../assets/img/hylet_logo_png.png';
import Image from 'next/image';
import Link from 'next/link'
import {useRouter} from 'next/router'

const {Search} = Input;

import {
    UserOutlined,
    MenuOutlined,
    ArrowRightOutlined, 
    CompassOutlined,
    BellOutlined,
    LogoutOutlined,
    PoweroffOutlined,
    ThunderboltOutlined,
    AimOutlined,
    SettingOutlined,
    DollarOutlined,
    FileProtectOutlined,
    DingtalkOutlined
} from '@ant-design/icons';


const LayoutComponent = (props)=> {
    const {children} = props;
    const isAuth = true;
    const Router = useRouter();

    // highlights current path link
    useEffect(()=>{
        const path = Router.pathname;
        if(path === '/'){
            toggleMenu("home");
        }else{
            const pathList = path.split('/');
            const currentPath = pathList[1];
            toggleMenu(currentPath);
        }
    },[Router.pathname])

    // handle search input
    const onSearch = value => console.log(value);
    
    // menu switching
    const toggleMenu = (e)=>{
        let listItems = document.getElementsByClassName('item');
        let activeItem = document.getElementById(e);
        for(let i = 0 ; i < listItems.length ; i++){
            listItems[i].classList.remove('nav-link-active')
        }
        if(activeItem)
            activeItem.classList.add('nav-link-active');
    }

    // hide and show side bar based on the viewport
    const showMenu = ()=>{
        document.getElementsByClassName('navigation')[0].classList.toggle('active');
    }

    // account menu
    const menu = (
        <Menu className='px-3'>
          <Menu.Item key='1' icon={<UserOutlined />}>
              Profile
          </Menu.Item>
          <Menu.Item key='2' icon={<LogoutOutlined />}>
              Logout
          </Menu.Item>
        </Menu>
      );
      

    return (
        <div className={'pb-5'}>
            <nav className={'navbar sticky-top navbar-expand-lg navbar-light bg-white border-bottom py-0'} style={{height:"44px"}}>
                <div className="container-fluid px-lg-4 brand-div w-100">
                    <div className="d-flex flex-row justify-content-between w-100">
                        <div className="menubar d-block d-lg-none mt-1">
                            <Button type="text" onClick={showMenu} icon={<MenuOutlined />}/>
                        </div>
                        <a href="#" className={'navbar-brand '}>
                            <div className="d-flex flex-row collapse justify-content-around">
                                <Image src={logo}  width="25" alt={'logo'} height="30"/>
                                <span className={'px-1 text-b '}>h y l e t</span>
                            </div>
                        </a>
                        <div className={'d-none d-lg-block mt-1 w-50'}>
                            <Search placeholder="Search" onSearch={onSearch} />
                        </div>
                        {!isAuth && 
                            <>
                                <div className="menubar d-block d-lg-none mt-1">
                                    <Link href='/auth' passHref>
                                        <Button type="text" >Sign in</Button>
                                    </Link>
                                </div>
                                
                                <div className={'d-none d-lg-block mt-1'}>
                                    <Link href='/auth' passHref>
                                        <Button type="text" >Sign in</Button>
                                    </Link>
                                    <span className={'text-disabled px-1'}> or </span>
                                    <Link href='/auth?type=signup' passHref>
                                        <Button type="text" >Sign up</Button>
                                    </Link>
                                </div>
                            </>
                        }

                        {isAuth && 
                            <>
                                {/* <div className="menubar d-block d-lg-none mt-1">
                                    <Button shape='circle'  icon={<UserOutlined />}/>
                                </div> */}
                                
                                <div className={'mt-1'}>
                                    <Button shape='circle'  icon={<BellOutlined />}/>
                                    <span className={'text-disabled px-1'}> </span>
                                    <Dropdown overlay={menu} placement="bottomRight" arrow>
                                        <Button shape='circle'  icon={<UserOutlined />}/>
                                    </Dropdown>
                                </div>
                            </>
                        }
                        
                    </div>
                </div>
            </nav>
            <div className="navigation">
                <ul className='menuList'>
                    <li  className='menu-link'>
                        <Link  href="/" passHref>
                            <div className="item" id='home'>
                                <span className="icon"><CompassOutlined /></span>
                                <span className="title pt-1" >Home</span>
                            </div>
                        </Link>
                    </li>
                    <li  className='menu-link'>
                        <Link  href="/top" passHref>
                            <div className="item"  id='top' >
                                    <span className="icon"><ThunderboltOutlined /></span>
                                    <span className="title pt-1">Top Picks</span>
                            </div>
                        </Link>
                    </li>
                    <li  className='menu-link'>
                        <Link  href="/adds" passHref>
                            <div className="item" id='adds'>
                                    <span className="icon"><AimOutlined /></span>
                                    <span className="title pt-1" >My Ads</span>
                            </div>
                        </Link>
                    </li>
                    <li className='menu-link'>
                        <Link  href="/pages" passHref >
                            <div className="item" id='pages'>
                                <span className="icon"><DingtalkOutlined /></span>
                                <span className="title pt-1" >My Pages</span>
                            </div>
                        </Link>
                    </li>
                    <li className='menu-link'>
                        <Link  href="/offers" passHref>
                            <div className="item"  id='offers'>
                                    <span className="icon"><FileProtectOutlined /></span>
                                    <span className="title pt-1">My Offers</span>
                            </div>
                        </Link>
                    </li>
                    <li className='menu-link'>
                        <Link  href="/payments" passHref>
                            <div className="item" id='payments'>
                                    <span className="icon"><DollarOutlined /></span>
                                    <span className="title pt-1" >Payments</span>
                            </div>
                        </Link>
                    </li>
                    
                    <li className='menu-link'>
                        <Link  href="/settings" passHref>
                            <div className="item" id='settings'>
                                    <span className="icon"><SettingOutlined /></span>
                                    <span className="title pt-1" >Settings</span>
                            </div>
                        </Link>
                    </li>
                    <li className='menu-link'>
                        <Link  href="#" passHref>
                            <div className="item" id='logout'>
                                    <span className="icon"><PoweroffOutlined /></span>
                                    <span className="title pt-1" >Logout</span>
                            </div>
                        </Link>
                    </li>
                </ul>
                <div className="toggle">
                    <span className="icon d-none d-lg-block">
                        <Button type="link" className={'mx-2'} onClick={showMenu} icon={<ArrowRightOutlined />}/>
                    </span>
                </div>
            </div>
            <div className="content px-2 px-lg-5 py-lg-4 py-3">
            <div className="container-fluid">
                <div className="row mb-3 mb-lg-0">
                    <div className="col-12">
                        <div className={'d-block d-lg-none w-100'}>
                            <Search placeholder="Search" onSearch={onSearch} />
                        </div>
                    </div>
                </div>
            </div>
                {children}
            </div>
        </div>
    );
}

export default LayoutComponent;