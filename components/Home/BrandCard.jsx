import React from 'react';
import Image from 'next/image';
import mock from '../../assets/img/mock.png'

function BrandCard(props) {
    return (
        <div className="mt-2">
            <Image preview={"false"} className={'rounded w-100'} height='140' layout='responsive' alt='logo' src={mock} />
            <div className={'mt-2'}>
                <div className="d-flex justify-content-between">
                    <p className={'text-d-s'}> Mokka Engineer</p>
                    <small>750K | Insta</small>
                </div>
                <p>₹ 500</p>
            </div>
        </div>
    );
}

export default BrandCard;