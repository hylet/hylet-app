import React from 'react';
import {  Button, Carousel, Input} from 'antd';
import BrandCard from "./BrandCard";
import ad1 from '../../assets/img/ad1.png';
import ad2 from "../../assets/img/ad2.png";
import ad3 from '../../assets/img/ad3.png';
import {CheckOutlined, VideoCameraOutlined} from "@ant-design/icons";
import Image from "next/image";


const Home = (props)=> {

    return (
        <div className="container my-3 text-secondary">
            
            <div className="row mt-3">
                <div className="col-12 col-lg-6">
                    <div className="display-6"> <span className='text-b'>Hylet</span> - One Stop Solution</div>
                    <div>for all your <span className='text-b'>digital marketing</span> needs</div>
                </div>
                <div className="col-6 col-lg-3 mt-3 mt-lg-0">
                    <div className={'mt-2'}>
                        Need Promotion ?
                        <Button type='primary' block className="mt-1">Create Ad</Button>
                    </div>
                </div>
                <div className="col-6 col-lg-3 mt-3 mt-lg-0">
                    <div className={'mt-2'}>
                        Content Creator ?
                        <Button type='primary' block className=" mt-1">Add Your Page</Button>
                    </div>
                </div>
            </div>
            <div className="mt-4 text-center">
                <Carousel autoplay>
                    <div>
                        <Image alt={'img'} className={'rounded'}  src={ad1} width={500} height={261}/>
                    </div>
                    <div>
                        <Image alt={'img'} className={'rounded'} src={ad2} width={500} height={261}/>
                    </div>
                    <div>
                        <Image alt={'img'} className={'rounded'}  src={ad3} width={500} height={261}/>
                    </div>
                </Carousel>
            </div>
            <div className="mt-5 ">
                <div className={'row'}>
                    <div className="col-12">
                        <div className="h5 text-center">
                            &quot; Connecting <strong className='text-b'>Content Creators</strong> and <strong className='text-b'>Influencers</strong> with Business Owners who are in need of Promotion &quot;
                        </div>
                    </div>
                    <div className="col-12 col-lg-5 mt-4 card py-3">
                        <div className="display-6 text-center">For Business Owners</div>
                        <div className="px-3 px-lg-5  mt-4"><p><CheckOutlined className={'text-b'} /> <span >Promote your Business within <strong>Minutes</strong></span> </p></div>
                        <div className="px-3 px-lg-5 mt-3"><p><CheckOutlined className={'text-b'}/> <span >Choose from the wide variety of Pages</span> </p></div>
                        <div className="px-3 px-lg-5 mt-3"><p><CheckOutlined className={'text-b'}/> <span >Get Reach feedback</span> </p></div>
                        <div className="mt-3 d-flex justify-content-around">
                            <Button type={'text'} icon={<VideoCameraOutlined className={'text-b'}/>}>Watch Video</Button>
                            <Button type='primary' className="px-5">Create Ad</Button>
                        </div>
                    </div>
                    <div className="col-lg-2">  </div>
                    <div className="col-12 col-lg-5 mt-4 card py-3">
                        <div className="display-6 text-center">For Content Creators</div>
                        <div className="px-3 px-lg-5 mt-4"><p><CheckOutlined className={'text-b'}/> <span >No more losing offers in DM&apos;s</span> </p></div>
                        <div className="px-3 px-lg-5 mt-3"><p><CheckOutlined className={'text-b'}/> <span >Reach more Customers</span> </p></div>
                        <div className="px-3 px-lg-5 mt-3"><p><CheckOutlined className={'text-b'}/> <span >Keep track of all your Promotions</span> </p></div>
                        <div className="mt-3 text-center">
                            <Button type='primary' className="px-5">Add My Page</Button>
                        </div>
                    </div>
                </div>
            </div>
            <div className=" mt-5">
                <h3 className={'text-secondary'}>Featured</h3>
            </div>
            <div className="d-flex flex-row justify-content-between mt-3 mb-1">
                <div>
                    <p className={'text-secondary'}>Most Followers </p>
                </div>
                <div className={'text-b'}>More</div>
            </div>
            <div className="row">
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
            </div>
            <div className="d-flex flex-row justify-content-between mt-5 mb-1">
                <div>
                    <p className={'text-secondary'}>Most Ads </p>
                </div>
                <div className={'text-b'}>More</div>
            </div>
            <div className="row">
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
                <div className="col-lg-3 col-12">
                    <BrandCard />
                </div>
            </div>
        </div>
    );
}


export default Home;