import React from 'react'
import Head from "next/head";
import { Divider } from 'antd';
import { CheckCircleTwoTone} from '@ant-design/icons';
import Image from 'next/image';
import mock from '../../assets/img/mock.png'
import PricingForm from './PricingForm';
import CurrentList from './CurrentList';
import OldList from './OldList';

const Profile = ()=> {      
    return (
        <div className='px-2 '>
            <Head>
                <title>Hylet - Kiddo</title>
                <meta name="Hylet" content="Marketing Simplified" />
                <link rel="icon" href="/hylet_logo.png" />
            </Head>
            <div className='titlebar rounded'>
                <div className="row ">
                    <div className="col-lg-2 col-3">
                        <Image preview={"false"} className={'rounded rounded-circle'} height='150' width='150'  alt='logo' src={mock} />
                    </div>
                    <div className="col-lg-4 col-9">
                        <div className={' px-3 mt-3'}>
                            <div className="d-flex justify-content-between">
                                <p className='mb-0 h5'> Mokka Engineer <span><CheckCircleTwoTone twoToneColor="#52c41a" /></span></p>
                            </div>
                            <small className='text-secondary'> active <span className='px-2 text-b'>|</span> visit page </small>
                            <div className="mt-3 d-flex justify-content-between">
                                <div>
                                    <span className='text-o'>1M </span>
                                    <span className='ml-3'>followers</span>
                                </div>
                                <div>
                                    <span className='text-o'>17 </span>
                                    <span className='ml-3'>ads</span>
                                </div>
                                <div>
                                    <span className='text-o'>2 </span>
                                    <span className='ml-3'>live</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="mt-4">
                    <PricingForm/>
                </div>

                <Divider/>

                <div >
                    <div className="mt-3 h5">
                        Currently Promoted Ads
                    </div>
                    <CurrentList/>
                </div>

                <Divider/>

                <div>
                    <div className="mt-3 h5">
                        Old Promotions
                    </div>
                    <OldList/>
                </div>
                

            </div>  
        </div>
    )
}

export default Profile;
