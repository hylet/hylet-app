import React from 'react';
import CurrentCard from './CurrentCard';


const CurrentList = ()=> {
    return (
        <div className='row mt-2'>
            <div className="col-12 col-lg-4">
                <CurrentCard />
            </div>
            <div className="col-12 col-lg-4">
                <CurrentCard/>
            </div>

        </div>
    )
}

export default CurrentList;