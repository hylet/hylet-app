import React from 'react';
import OldCard from './OldCard';


const OldList = ()=> {
    return (
        <div className='row mt-2'>
            <div className="col-12 col-lg-4">
                <OldCard />
            </div>
            <div className="col-12 col-lg-4">
                <OldCard/>
            </div>

        </div>
    )
}

export default OldList;