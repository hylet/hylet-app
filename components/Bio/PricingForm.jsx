import React from 'react'
import { Table ,Button, Form, Select} from 'antd';

const { Option } = Select;

export default function PricingForm() {
    const columns = [
        {
            title: 'Type',
            dataIndex: 'type',
            render: (text) => <a>{text}</a>,
        },        
        {
            title: 'Day(s)',
            dataIndex: 'days',
        },
        {
            title: 'Price',
            dataIndex: 'price',
        },
    ];

    const data = [
        {
          key: '1',
          type: 'Post',
          days: 1,
          price: "₹ 1000",
        },
        {
            key: '2',
            type: 'Story',
            days: 1,
            price: "₹ 1000",
        },
        {
            key: '3',
            type: 'Story + Post',
            days: 1,
            price: "₹ 1500",
        },
    ];

    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
          console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
      };


    const onFinish = () => {
        console.log('Success:', values);
      };
    
      const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
      };
      


    return (
        <Form
            name="basic"
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
        >
            
            <div className="d-flex justify-content-between mb-3">
                    <div className="h5">Pricing</div>
            </div>

            <Table
                    rowSelection={{
                        type: 'radio',
                        ...rowSelection,
                    }}
                    pagination={false}
                    columns={columns}
                    dataSource={data} />

            <Form.Item name="adName" rules={[{ required: true, message: 'Please choose the Ad!', }]}>
                <Select className="d-lg-block mt-4 w-50" 
                    placeholder="Choose the required ad"
                    allowClear>

                    <Option value="ad1">Ad1</Option>
                    <Option value="ad2">Ad2</Option>

                </Select>
            </Form.Item>

            <Form.Item>
                <Button type="primary" htmlType="submit">
                    Add to Cart
                </Button>
            </Form.Item>
        </Form>
    )
}
