import React from 'react';
import Image from 'next/image';
import mock from '../../assets/img/mock.png'
import Link from 'next/link';
import {Button} from 'antd';
const OfferCard = ()=> {
    return (
        <div className="col-12 col-lg-4">
            <div className="mt-3 card">
                <div className="d-flex justify-content-start card-header">
                    <Image preview={"false"} className={'rounded w-100 rounded-circle'} height='50' width='50' alt='logo' src={mock} />
                    <Link href='/pages/kiddo' passHref>
                        <div className={' px-3 mt-3'}>
                            <p className='mb-0 hv-b pointer'> Hylet </p>
                        </div>
                    </Link>
                </div>
                <div className="card-body ">
                    <div className="d-flex justify-content-between">
                        <p>New Launch in Chennai</p>
                        <p><strong>₹ 300</strong></p>
                    </div>
                    <div className="d-flex justify-content-between mt-1 ">
                        <p className='text-secondary mt-1'>Type: <strong>Post</strong></p>
                        <Link href='/offers/12345' passHref>
                            <Button type='primary'>View Offer</Button>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default OfferCard;