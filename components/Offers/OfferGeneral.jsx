import React from 'react'
import { Descriptions, Badge, Image } from 'antd';
const OfferGeneral = ()=> {
    return (
        <div>
            <div className="d-flex justify-content-between mb-3">
                <div className="h6">Ad Details</div>
            </div>
            <div className='mb-3'>
                <Image.PreviewGroup>
                    <Image width={200} src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg" />
                    <Image
                    width={200}
                    src="https://gw.alipayobjects.com/zos/antfincdn/aPkFc8Sj7n/method-draw-image.svg"
                    />
                </Image.PreviewGroup>
            </div>
            <Descriptions bordered column={1}>
                <Descriptions.Item label="Ad Name">New Launch</Descriptions.Item>
                <Descriptions.Item label="Ad Description">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Numquam commodi dicta illo exercitationem veritatis, assumenda minima magni corporis iste? Hic atque quasi tempore placeat corporis dolores quos neque! Dolor, fugit?</Descriptions.Item>
                <Descriptions.Item label="Post Caption">We are here #we the people</Descriptions.Item>
                <Descriptions.Item label="Website Link">www.onerepublic.com</Descriptions.Item>
                <Descriptions.Item label="Profile Link">oner4ever</Descriptions.Item>         
            </Descriptions>
            
        </div>
    )
}

export default  OfferGeneral;